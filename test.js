const fs = require("fs");
const path = require("path");

const sumPairs = (arr, sum) => {
  const stack = {};
  for (let i in arr) {
    const num = sum - arr[i];
    if (typeof stack[num] !== "undefined") {
      return {
        values: [num, arr[i]],
        indexes: [stack[num], i],
      };
    }
    
    stack[arr[i]] = stack[arr[i]] || i;
  }
}

const sumPairs2 = (arr, sum) => {
	const stack = {};

  for (let i = 0; i < arr.length; i++) {
    const num = sum - arr[i];
    if (typeof stack[num] !== "undefined") {
      return {
        values: [num, arr[i]],
        indexes: [stack[num], i],
      };
    }
    
    stack[arr[i]] = stack[arr[i]] || i;
  }

  return {
    values: undefined,
    indexes: undefined,
  };
};

class Test {
	constructor() {
		this.functions = [];
		this.results = [];
	}

	add(name, fn) {
		this.functions.push({ name, fn });
	}

	async run(iterations) {
		this.results = [];
		for(const f of this.functions) {
			await this.test(f.name, f.fn, iterations);
		}
		console.clear();
		console.log(`Iterations: ${iterations}`);
		this.results.forEach(f => {
			console.log(`${f.name}: ${f.result}ms, average: ${f.average}ms`);
		});
	}

	async test(name, fn, iterations) {
		const data = await fs.readFileSync(path.join(__dirname, "data.txt"), "utf8");
		const arr = data.split(",").map(e => +e);

		let result = fn([1,2,3,4], 5);
		if (result.values.join(",") !== "2,3") {
			console.log("Wrong values");
			return;
		}
		if (result.indexes.join(",") !== "1,2") {
			console.log("Wrong indexes");
			return;
		}

		const date = +new Date();
		for (let i = 0; i < iterations; i++) {
			console.clear();
			console.log(`Runing ${name}, iteration ${i+1}`);
			fn(arr, 97_653_293);
			fn(arr, 97_653);
			fn(arr, 197_653);
		}
		const dateEnd = +new Date();

		this.results.push({
			name,
			result: dateEnd - date,
			average: +((dateEnd - date) / iterations / 3).toFixed(2) 
		});
	}
}

const test = new Test();
test.add("Dima's function 1", sumPairs);
test.add("Dima's function 2", sumPairs2);

test.run(2);
